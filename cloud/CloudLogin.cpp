#include "CloudLogin.h"
#include <QFile>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QNetworkReply>
#include <QNetworkAccessManager>
//#include <QDesktopServices>
#include <QDir>
#include <QProgressDialog>
#include <QApplication>
#include <QMessageBox>
#include <QDebug>

CloudLogin::CloudLogin(QWidget *parent)
    : QDialog(parent)
    , m_first_state(true)
{
    setFixedSize(400, 250);

    QLabel* lbl_choice = new QLabel("Select drive:");
    cb_disk = new QComboBox;
    cb_disk->addItem(QPixmap("../Doc/Icon/yandex"), "Yandex");

    m_login = new LoginUI;
    m_login->setVisible(false);
    m_link = new LinkUI;
    m_anim_button = new AnimationButton( width() - 30, 50 );
    m_anim_button->setLabelFont(true);

    QHBoxLayout* lo_clouddisk = new QHBoxLayout;
    lo_clouddisk->addWidget(lbl_choice);
    lo_clouddisk->addWidget(cb_disk, 1);

    pb_cancel = new QPushButton("Cancel");
    pb_ok = new QPushButton("Get / Open");
    QHBoxLayout* lo_button = new QHBoxLayout;
    lo_button->addWidget(pb_cancel);
    lo_button->addWidget(pb_ok);

    QVBoxLayout* lo_all = new QVBoxLayout;
    lo_all->addWidget(m_anim_button);
    lo_all->addLayout(lo_clouddisk);
    lo_all->addWidget(m_login);
    lo_all->addWidget(m_link);
    lo_all->addLayout(lo_button);

    setLayout(lo_all);

    setWindowTitle("Open from \"cloud\" disk");
    setWindowIcon(QPixmap("../Doc/Icon/cloud"));

    connect(pb_cancel, &QPushButton::clicked, this, &QDialog::reject);
    connect(pb_ok, &QPushButton::clicked, this, &CloudLogin::onOkClicked);   
    connect(m_anim_button, &AnimationButton::clicked, this, &CloudLogin::onStateChanged);

    m_pnam = new QNetworkAccessManager(this);
}

void CloudLogin::onStateChanged()
{    
    m_first_state = !m_first_state;

    pb_ok->setText(m_first_state ? "Get / Open" : "Login");
    m_link->setVisible(m_first_state);
    m_login->setVisible(!m_first_state);
    m_anim_button->setLabelFont(m_first_state);    

    // For Google/Dropbox link:
    /*cb_disk->clear();
    if (m_first_state)
    {
        cb_disk->addItem(QPixmap("../Doc/Icon/yandex"), "Yandex");
        cb_disk->addItem(QPixmap("../Doc/Icon/google"), "Google");
        cb_disk->addItem(QPixmap("../Doc/Icon/dropbox"), "Dropbox");
    }
    else
    {
        cb_disk->addItem(QPixmap("../Doc/Icon/yandex"), "Yandex");
    }*/
}

void CloudLogin::onOkClicked()
{
    if ( m_first_state )
    {
        if ( m_link->getLink().isEmpty() )
        {
            QMessageBox::information(this, "Information", "Specify a link to the file!");
            return;
        }

        QNetworkRequest rqs( QUrl(getStringUrl(), QUrl::StrictMode) );    // right mode?
        rqs.setAttribute( QNetworkRequest::FollowRedirectsAttribute, true );
        rqs.setMaximumRedirectsAllowed(3);
        QNetworkReply* rpl = m_pnam->get(rqs);

        //m_prbar = new QProgressDialog("File download...", "&Cancel", 0, 100, this);
        //m_prbar->setMinimumDuration(0);
        //m_prbar->setAutoClose(true);
        //m_prbar->setWindowTitle("Please wait");

        connect(rpl, &QNetworkReply::finished, this, &CloudLogin::slotDownload);
        //connect(rpl, &QNetworkReply::downloadProgress, this, &CloudLogin::slotDownloadProgress);
    }
    else
    {
        if ( m_login->getUsername().isEmpty() || m_login->getPassword().isEmpty() )
        {
            QMessageBox::information(this, "Information", "Enter all fields!");
        }
        else
        {
            QDialog::accept();  // accept - значит ввел логин и пароль, и хочешь войти в диск
        }
    }
}
// получить ссылку для скачивания (когда качаешь по прямой ссылке)
QString CloudLogin::getStringUrl() const
{
    QString download_link = m_link->getLink();
    QString yandex_host = "https://getfile.dokpub.com/yandex/get/";
    // QString google_host = "https://drive.google.com/uc?export=download&id=";

    if ( cb_disk->currentText() == "Yandex" )
    {
        download_link.prepend(yandex_host);
    }    
    // For Google/Dropbox link:
    /*
    else if ( cb_disk->currentText() == "Dropbox" )
    {
        if( download_link.back() != '1' )
        {
            download_link[download_link.length() - 1] = '1';
        }
    }
    else if ( cb_disk->currentText() == "Google" )
    {
        if( !download_link.contains('/') )
            return (google_host + download_link);

        for( int i = 0; i < download_link.length(); i++ )
        {
            if( !download_link[i].isDigit() )
                continue;

            download_link.remove(0, i);
            break;
        }

        for( int i = 0; i < download_link.length(); i++ )
        {
            if( download_link[i] != '/' )
                continue;

            download_link.remove(i, download_link.length() - i);
        }

        download_link.prepend(google_host); // == download_link = google_host + download_link;
    }
    */

    return download_link;
}
/* Dont work (
void CloudLogin::slotDownloadProgress(qint64 nReceived, qint64 nTotal)
{
    if (nTotal <= 0)
        return;

    if (!m_prbar)
        return;

    QApplication::processEvents();

    m_prbar->setValue(100*nReceived/nTotal);
}
*/
void CloudLogin::slotDownload()
{
    QNetworkReply* reply = static_cast<QNetworkReply*>(sender());
    if (!reply)
        return;

    QByteArray file = reply->readAll(); // - это и есть файл, скаченный по ссылке

    // qDebug() << reply->url().toString(); - отсюда можно узнать имя файла

    // For download file on local PC:
    /*
    m_download_path = QDir::currentPath();
    for(int i = m_download_path.length() - 1; i >= 0 ; i--)
    {
        if(m_download_path[i] != '/')
            continue;

        m_download_path.remove(i + 1, m_download_path.length() - 1);
        break;
    }
    m_download_path += "Download/";

    QString str_name;
    if(cb_disk->currentText() == "Dropbox")
    {
        str_name = createFileNameDropbox();
    }
    else if(cb_disk->currentText() == "Google")
    {
        str_name = "Google_name.jpg";        
    }
    else if(cb_disk->currentText() == "Yandex")
    {
        str_name = "Yandex_name.jpg";        
    }

    QFile file(m_download_path + str_name);
    if(file.open(QIODevice::WriteOnly))
    {
        file.write(reply->readAll());
    }
    file.close();
    */

    QDialog::done(0);   // файл скачан, можно закрывать окно (done, потому что accept для входа в диск)
}
/*
QString CloudLogin::createFileNameDropbox()
{
    m_file_extansion = m_link->getLink();
    for(int i = m_file_extansion.length() - 1; i >= 0; i--)
    {
        if(m_file_extansion[i] != '.')
            continue;

        m_file_extansion.remove(0, i);
        int pos = m_file_extansion.indexOf('?');
        if(pos != -1)
            m_file_extansion.remove(pos, m_file_extansion.length() - 1);
        break;
    }

    m_file_name = m_link->getLink();
    for(int i = m_file_name.length() - 1; i >= 0; i--)
    {
        if(m_file_name[i] != '/')
            continue;

        m_file_name.remove(0, i + 1);
        int pos = m_file_name.indexOf('.');
        if(pos != -1)
            m_file_name.remove(pos, m_file_name.length() - 1);
        break;
    }

    size_t index = 0;
    while( QFile::exists(m_download_path + m_file_name + m_file_extansion) )
    {
        index++;
        m_file_name += QString::number(index);
    }

    return m_file_name + m_file_extansion;    
}
*/
QString CloudLogin::getDiskName() const
{
    return cb_disk->currentText();
}
