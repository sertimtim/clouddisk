#include "MenuBar.h"
#include <QMenu>
#include <QMenuBar>
#include <QLabel>
#include <QPushButton>
#include <QLayout>
#include <QMouseEvent>

MenuBar::MenuBar(QWidget* parent) : QWidget(parent)
{
    m_file = new QMenu("&File", this);
    a_cloud = new QAction("Open from Yandex", this);
    a_cloud->setIcon(QPixmap("../Doc/Icon/cloud"));
    a_cloud->setToolTip("Load file from cloud disk");
    m_file->addAction(a_cloud);
}
