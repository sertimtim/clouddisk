#ifndef MENUBAR_H
#define MENUBAR_H

#include <QWidget>

class QMenu;

// это формальный класс с меню
class MenuBar : public QWidget
{
public:
    explicit MenuBar(QWidget *parent = nullptr);
    ~MenuBar() = default;

    QMenu* getMenu() const { return m_file; }
    QAction* getAction() const { return a_cloud; }

private:
    QMenu* m_file;
    QAction* a_cloud;    
};

#endif // MENUBAR_H
