#pragma once

#include <QWidget>
#include <QLabel>

class QPushButton;
class QMouseEvent;

// Класс "надпись" которая посылает сигнал, если по ней кликнуть
// Используется в AnimationButton
class LabelClick : public QLabel
{
    Q_OBJECT
public:
    explicit LabelClick(const QString& text, QWidget* parent = nullptr);
    ~LabelClick() = default;

signals:
    void clicked();

protected:
    virtual void mousePressEvent(QMouseEvent *e) override;
};

// класс с анимационной кнопкой, для выбора как скачивать файл: по открытой ссылке или войти в яндекс.диск
// используется в CloudLogin
class AnimationButton : public QWidget
{
    Q_OBJECT
public:
    explicit AnimationButton(int n_width, int n_height, QWidget *parent = nullptr);
    ~AnimationButton() = default;    

    void setLabelFont(bool first_state);

signals:
    void clicked();

private:
    QPushButton* pb_state;
    LabelClick* lbl_link;
    LabelClick* lbl_login;
};

