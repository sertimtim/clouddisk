#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "MenuBar.h"
#include "CloudLogin.h"
#include "CloudFileDialog.h"

class MenuBar;
class QScrollArea;

class MainWindow : public QMainWindow
{
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void slotOpenFromCloud();

private:
    MenuBar* mb;
    QScrollArea* scrl;
};

#endif // MAINWINDOW_H
