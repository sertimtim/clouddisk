#pragma once

#include <QDialog>
#include "LoginUI.h"
#include "LinkUI.h"
#include "AnimationButton.h"

class QComboBox;
class QLineEdit;
class QNetworkReply;
class QNetworkAccessManager;
class QProgressDialog;

// класс - дилоговое окно, для выбора как открыть файл (по прямой ссылке (LinkUI) или ввести логин и пароль и войти в диск (LoginUI))
class CloudLogin : public QDialog
{
public:
    explicit CloudLogin(QWidget *parent = nullptr);
    ~CloudLogin() = default;

    QString getDiskName() const;
    const LoginUI* login() const { return m_login; }    

public slots:
    void onStateChanged();
    void onOkClicked();
    void slotDownload();    
    void slotDownloadProgress(qint64 nReceived, qint64 nTotal);

private:
    QString getStringUrl() const;
    QString createFileNameDropbox();

private:
    QComboBox* cb_disk;

    AnimationButton* m_anim_button;
    LoginUI* m_login;
    LinkUI* m_link;

    QPushButton* pb_cancel;    
    QPushButton* pb_ok;

    QNetworkAccessManager* m_pnam;
    QProgressDialog* m_prbar;

    //QString m_download_path;
    //QString m_file_name;
    //QString m_file_extansion;

    // первое состояние - скачать файл по ссылке, второе - войти в диск
    bool m_first_state;
};
