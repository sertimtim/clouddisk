#include "LoginUI.h"
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QLayout>

LoginUI::LoginUI(QWidget *parent)
    : QWidget(parent)
    , password_mode(true)
{
    QLabel* lbl_login = new QLabel("&User name:");
    QLabel* lbl_password = new QLabel("&Password");
    le_login = new QLineEdit;
    le_login->setWhatsThis("Enter the \"username\" from the Disk.Yandex service");
    le_password = new QLineEdit;
    le_password->setWhatsThis("Enter the password from the Disk.Yandex service");
    le_password->setEchoMode(QLineEdit::Password);
    lbl_login->setBuddy(le_login);
    lbl_password->setBuddy(le_password);

    pb_showpassword = new QPushButton;
    pb_showpassword->setFlat(true);
    pb_showpassword->setIcon(QPixmap("../Doc/Icon/eye"));
    pb_showpassword->setToolTip("Show/Hide password");

    QGridLayout* lo_login = new QGridLayout;
    lo_login->addWidget(lbl_login, 1, 0);
    lo_login->addWidget(le_login, 1, 1, 1, 2);
    lo_login->addWidget(lbl_password, 2, 0);
    lo_login->addWidget(le_password, 2, 1);
    lo_login->addWidget(pb_showpassword, 2, 2);

    setLayout(lo_login);

    connect(pb_showpassword, &QPushButton::clicked, this, &LoginUI::showPassword);
}

void LoginUI::showPassword()
{
    le_password->setEchoMode(password_mode ? QLineEdit::Normal : QLineEdit::Password);
    password_mode = !password_mode;
}

QString LoginUI::getUsername() const
{
    return le_login->text();
}

QString LoginUI::getPassword() const
{
    return le_password->text();
}
