#include "AnimationButton.h"
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QStateMachine>
#include <QPropertyAnimation>
#include <QSignalTransition>
#include <QMouseEvent>

LabelClick::LabelClick(const QString& text, QWidget* parent) : QLabel(parent)
{
    setText(text);
}

void LabelClick::mousePressEvent(QMouseEvent *e)
{
    if(e->button() == Qt::LeftButton)
    {
        emit clicked();
    }
}

AnimationButton::AnimationButton(int n_width, int n_height, QWidget *parent) : QWidget(parent)
{
    lbl_link = new LabelClick("Get file by direct link");
    lbl_login = new LabelClick("Get file from your cloud drive");

    QVBoxLayout* lo_label = new QVBoxLayout;    
    lo_label->setContentsMargins(n_height + 5, 5, 5, 5);
    lo_label->addWidget(lbl_link);
    lo_label->addStretch(1);
    lo_label->addWidget(lbl_login);

    setLayout(lo_label);
    setFixedSize(n_width, n_height);

    pb_state = new QPushButton(this);
    pb_state->setFlat(true);
    pb_state->setCursor(QCursor(Qt::CursorShape::PointingHandCursor));
    pb_state->setIcon(QPixmap("../Doc/Icon/right"));
    pb_state->setIconSize( QSize(pb_state->width(), pb_state->height() - 15) );    
    pb_state->show();    

    QStateMachine* psm = new QStateMachine(this);

    QState* state_link = new QState(psm);
    QRect rect1(0, 0, n_height, n_height/2);
    state_link->assignProperty(pb_state, "geometry", rect1);   
    psm->setInitialState(state_link);

    QState* state_login = new QState(psm);
    QRect rect2(0, n_height/2, n_height, n_height/2);
    state_login->assignProperty(pb_state, "geometry", rect2);    

    QSignalTransition* ptrans1 = state_link->addTransition(pb_state, &QPushButton::clicked, state_login);
    QSignalTransition* ptrans2 = state_login->addTransition(pb_state, &QPushButton::clicked, state_link);
    QPropertyAnimation* panim1 = new QPropertyAnimation(pb_state, "geometry", this);
    ptrans1->addAnimation(panim1);
    QPropertyAnimation* panim2 = new QPropertyAnimation(pb_state, "geometry", this);
    ptrans2->addAnimation(panim2);
    psm->start();

    connect(pb_state, &QPushButton::clicked, this, &AnimationButton::clicked);
    connect(lbl_link, &LabelClick::clicked, pb_state, &QPushButton::click);
    connect(lbl_login, &LabelClick::clicked, pb_state, &QPushButton::click);
}

void AnimationButton::setLabelFont(bool first_state)
{
    lbl_link->setFont( first_state ? QFont("Times", 8, QFont::Bold) : QFont("Times", 8, QFont::Normal) );
    lbl_login->setFont( first_state ? QFont("Times", 8, QFont::Normal) : QFont("Times", 8, QFont::Bold) );
}
