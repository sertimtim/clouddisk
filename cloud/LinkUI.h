#pragma once

#include <QWidget>

class QLineEdit;

// графический интерфейс для CloudLogin, если нужно скачать файл по ссылке
class LinkUI : public QWidget
{
public:
    explicit LinkUI(QWidget *parent = nullptr);
    ~LinkUI() = default;

    QString getLink() const;

private:
    QLineEdit* le_link;
};
