#include "MainWindow.h"
#include <QMenuBar>
#include <QMessageBox>
#include <QScrollArea>
#include <QLabel>
#include <QLayout>
#include <QPushButton>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    resize(800, 600);

    mb = new MenuBar(this);    
    menuBar()->addMenu(mb->getMenu());

    scrl = new QScrollArea;
    setCentralWidget(scrl);

    connect(mb->getAction(), &QAction::triggered, this, &MainWindow::slotOpenFromCloud);
}

void MainWindow::slotOpenFromCloud()
{
    CloudLogin* m_login = new CloudLogin(this);

    if( m_login->exec() == QDialog::Accepted ) // если нажали окей - хотят войти в яндекс.диск
    {
        CloudFileDialog* m_openfile = new CloudFileDialog(m_login->getDiskName(), m_login->login()->getUsername(), m_login->login()->getPassword(), this);

        if( m_openfile->exec() == QDialog::Accepted) // accept - если файл скачался - можно его открывать
        {
            if( m_openfile->getFileName().contains("jpg") || m_openfile->getFileName().contains("png") )
            {
                QLabel* lbl = new QLabel;
                lbl->setPixmap( QPixmap(m_openfile->getDownloadPath() + m_openfile->getFileName() + m_openfile->getFileExtansion()) );
                scrl->setWidget(lbl);
            }
        }
        else if( !m_openfile->setConnect() ) // если не удалось залогиниться к диску
        {
            QMessageBox::information(this, "Information", "Wrong login or password!");
        }
    }
}

MainWindow::~MainWindow()
{

}
