#include "CloudFileDialog.h"
#include <QTreeWidget>
#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QLayout>
#include <QHeaderView>
#include <QMessageBox>
#include <QDesktopServices>
#include <QKeyEvent>
#include <QProgressBar>
#include <QLabel>
#include <QComboBox>

CloudFileDialog::CloudFileDialog(const QString& cloud, const QString& username, const QString& password, QWidget *parent) : QDialog(parent),
    m_connect(false)
{
    m_path = "/";

    w_tree = new QTreeWidget;
    QStringList lst;
    lst << "Folders/Files" << "Change date" << "Used space";
    w_tree->setHeaderLabels(lst);
    w_tree->header()->resizeSection(0, 350);

    le_path = new QLineEdit;
    le_path->setReadOnly(true);
    le_path->setText(m_path);

    pb_up = new QPushButton;
    pb_up->setIcon(QPixmap("../Doc/Icon/up"));
    pb_up->setToolTip("Up (\"Backspace\")");
    pb_up->setFlat(true);
    pb_up->setStyleSheet("QPushButton:hover{background: rgb(0,198,198,32); border: 1px solid rgb(0,198,198,92);}"
                         "QPushButton:pressed{background: rgb(0,198,198,64); border: 1px solid rgb(0,198,198,92);}");

    QHBoxLayout* lo_path = new QHBoxLayout;
    lo_path->addWidget(pb_up);
    lo_path->addWidget(le_path);

    QLabel* lbl_filename = new QLabel("File name:");
    le_filename = new QLineEdit;
    le_filename->setReadOnly(true);
    cb_filetype = new QComboBox;
    _fillComboBoxType();

    QHBoxLayout* lo_type = new QHBoxLayout;
    lo_type->addWidget(lbl_filename);
    lo_type->addWidget(le_filename, 2);
    lo_type->addWidget(cb_filetype, 1);

    cb_openfolder = new QCheckBox("Open download directory");    
    prb_download = new QProgressBar;
    QLabel* lbl_prb = new QLabel("Download:");
    pb_open = new QPushButton("Get / Open");
    pb_cancel = new QPushButton("Cancel");
    QFrame* frame = new QFrame;
    frame->setFrameStyle(QFrame::VLine | QFrame::Raised);

    QHBoxLayout* lo_button = new QHBoxLayout;
    lo_button->addWidget(cb_openfolder);
    lo_button->addWidget(frame);
    lo_button->addWidget(lbl_prb);
    lo_button->addWidget(prb_download);
    lo_button->addStretch(1);
    lo_button->addWidget(pb_cancel);
    lo_button->addWidget(pb_open);

    QVBoxLayout* lo_all = new QVBoxLayout;
    lo_all->addLayout(lo_path);
    lo_all->addWidget(w_tree);
    lo_all->addLayout(lo_type);
    lo_all->addLayout(lo_button);

    setLayout(lo_all);

    resize(640, 480);

    if(cloud == "Yandex")
    {
        w.setConnectionSettings(QWebdav::HTTPS, "webdav.yandex.ru", "/", username, password, 443);
        setWindowIcon(QPixmap("../Doc/Icon/yandex"));
        setWindowTitle("Your disk.yandex.ru");
    }    

    connect(&p, &QWebdavDirParser::finished, this, &CloudFileDialog::printFolderList);
    connect(w_tree, &QTreeWidget::itemActivated, this, &CloudFileDialog::selectItem);
    connect(w_tree, &QTreeWidget::itemSelectionChanged, this, &CloudFileDialog::changeItem);
    connect(cb_filetype, &QComboBox::currentTextChanged, this, &CloudFileDialog::printFolderList);
    connect(pb_up, &QPushButton::clicked, this, &CloudFileDialog::clickUp);
    connect(pb_open, &QPushButton::clicked, this, &CloudFileDialog::clickOK);
    connect(pb_cancel, &QPushButton::clicked, this, &CloudFileDialog::reject);

    getFolderList();
}

void CloudFileDialog::keyPressEvent(QKeyEvent* pe)
{
    if(pe->key() == Qt::Key_Backspace)
        clickUp();
}

// заполнить комбокос-фильтр нужными типами файлов
void CloudFileDialog::_fillComboBoxType()
{
    QStringList lst;
    lst << "All files (*.*)" << "C3D (*.c3d)" << "STEP (*.step *.stp)" << "Parasolid (*.x_t *.x_b)";
    lst << "IGES (*.igs *.iges *.ige)" << "ACIS (*.sat)" << "VRML (*.vrml *.wrl)" << "STL (*.stl)" << "PLY(*.ply)";
    lst  << "JT (*.jt)" << "Wavefront Object (*.obj)" << "Object File Format (*.off)";
    cb_filetype->addItems(lst);
}

// получить список файлов и папок в текущей директории m_path
// по окончанию посылает сигнал финишед -> слот printFolderList
void CloudFileDialog::getFolderList()
{
    p.listDirectory(&w, m_path);
}

// выводит список файлов и папок
void CloudFileDialog::printFolderList()
{
    QList<QWebdavItem> list = p.getList();    

    // это проверка (не самая удачная), введен ли правильный логин и пароль
    // если в корневом каталоге у тебя пусто - либо у тебя пустой яндекс диск, либо не удалось войти
    if(!m_connect)
    {
        if(list.isEmpty())
            QDialog::reject();
        else
            m_connect = true;
    }

    w_tree->clear();

    QString current_type = cb_filetype->currentText();

    QTreeWidgetItem* item = nullptr;
    for(auto it = list.begin(); it != list.end(); it++)
    {
        QString file_type = it->name();

        if( cb_filetype->currentIndex() != 0 )
        {
            int i = file_type.indexOf('.');
            if(i != -1)
                file_type.remove(0, i + 1);
        }

        if( current_type.contains(file_type) || cb_filetype->currentIndex() == 0 || it->isDir() )
        {
            item = new QTreeWidgetItem(w_tree);
            item->setText(0, it->name());
            if( it->isDir() )
            {
                item->setIcon(0, QPixmap("../Doc/Icon/folder"));
                item->setText(1, it->lastModifiedStr());
            }
            else
            {
                item->setText(1, it->lastModifiedStr());
                item->setText(2, QString::number(it->size()/1024.0/1024.0, 'f', 2) + QString(" MB"));
            }
        }
    }
}

// двойной щелчок или ентер на файле или папке
void CloudFileDialog::selectItem(QTreeWidgetItem* tw_item)
{    
    QList<QWebdavItem> list = p.getList();

    auto it = std::find_if(list.begin(), list.end(), [tw_item] (const QWebdavItem& wd_item)
    { return wd_item.name() == tw_item->text(0); });

    if( it->isDir() )
    {
        m_path += it->name() + '/';
        le_path->setText(m_path);

        getFolderList();
    }
    else
    {
        QNetworkReply* reply = w.get(it->path());
        connect(reply, &QNetworkReply::finished, this, &CloudFileDialog::downloadFile);
        connect(reply, &QNetworkReply::downloadProgress, this, &CloudFileDialog::slotDownloadProgress);
    }
}

// одиночный клик на файле
void CloudFileDialog::changeItem()
{
    QTreeWidgetItem* item_current = nullptr;
    item_current = w_tree->currentItem(); // selectedItems - return QList

    QList<QWebdavItem> list = p.getList();

    auto it = std::find_if(list.begin(), list.end(), [item_current] (const QWebdavItem& wd_item)
    { return wd_item.name() == item_current->text(0); });

    if( it != list.end() )
    {
        if( !(it->isDir()) )
        {
            le_filename->setText(it->name());
        }
    }
}

// при нажатии на кнопку окей
void CloudFileDialog::clickOK()
{
    QTreeWidgetItem* item_current = nullptr;
    item_current = w_tree->currentItem();

    if (!item_current)
        QMessageBox::information(this, "Information", "Select directory or file");
    else
    {
        QList<QWebdavItem> list = p.getList();

        auto it = std::find_if(list.begin(), list.end(), [item_current] (const QWebdavItem& wd_item)
        { return wd_item.name() == item_current->text(0); });

        if( it->isDir() )
        {
            m_path += it->name() + '/';
            le_path->setText(m_path);

            getFolderList();
        }
        else
        {
            QNetworkReply* reply = w.get(it->path());
            connect(reply, &QNetworkReply::finished, this, &CloudFileDialog::downloadFile);
            connect(reply, &QNetworkReply::downloadProgress, this, &CloudFileDialog::slotDownloadProgress);
        }
    }
}

void CloudFileDialog::slotDownloadProgress(qint64 nReceived, qint64 nTotal)
{
    if (nTotal <= 0)
        return;

    prb_download->setValue(100 * nReceived / nTotal);
}

void CloudFileDialog::clickUp()
{
    if(m_path != '/')
    {        
        m_path.remove(m_path.length() - 1, 1);
        for(int i = m_path.length() - 1; i >=0 ; i--)
        {
            if(m_path[i] != '/')
                continue;
            m_path.remove(i + 1, m_path.length() - 1);
            break;
        }
        le_path->setText(m_path);

        getFolderList();
    }
}

// скачивает файл. Весь текст для скачивания файла на комп, если качать не нужно, то просто: reply->readAll()
void CloudFileDialog::downloadFile()
{
    QNetworkReply* reply = static_cast<QNetworkReply*>(sender());
    if (reply == nullptr)
        return;

    file_extansion = reply->url().toString(QUrl::RemoveUserInfo);
    for(int i = file_extansion.length() - 1; ; i--)
    {
        if(file_extansion[i] != '.')
            continue;

        file_extansion.remove(0, i);
        break;
    }

    file_name = reply->url().toString(QUrl::RemoveUserInfo);
    for(int i = file_name.length() - 1; ; i--)
    {
        if(file_name[i] != '/')
            continue;

        file_name.remove(0, i + 1);
        int pos = file_name.indexOf(file_extansion);
        if(pos != -1)
            file_name.remove(pos, file_name.length() - 1);
        break;        
    }    

    download_path = QDir::currentPath();
    for(int i = download_path.length() - 1; ; i--)
    {
        if(download_path[i] != '/')
            continue;

        download_path.remove(i + 1, download_path.length() - 1);
        break;
    }
    download_path += "Download/";

    size_t index = 0;
    while( QFile::exists(download_path + file_name + file_extansion) )
    {
        index++;
        file_name += QString::number(index);
    }

    QFile file(download_path + file_name + file_extansion);
    if(file.open(QIODevice::WriteOnly))
    {
        file.write(reply->readAll());
    }
    file.close();

    if(cb_openfolder->isChecked())
        QDesktopServices::openUrl(QUrl(download_path));

    QDialog::accept();
}

