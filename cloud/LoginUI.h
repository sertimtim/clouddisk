#pragma once

#include <QWidget>

class QLineEdit;
class QPushButton;

// графический интерфейс для CloudLogin, если нужно войти в яндекс.диск (ввести логин и пароль)
class LoginUI : public QWidget
{
public:
    explicit LoginUI(QWidget *parent = nullptr);
    ~LoginUI() = default;

    QString getUsername() const;
    QString getPassword() const;

public slots:
    void showPassword();

private:
    QLineEdit* le_login;
    QLineEdit* le_password;
    QPushButton* pb_showpassword;

    bool password_mode;
};

