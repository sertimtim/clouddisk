#ifndef CLOUDFILEDIALOG_H
#define CLOUDFILEDIALOG_H

#include <QDialog>

#include <qwebdav.h>
#include <qwebdavdirparser.h>
#include <qwebdavitem.h>

class QTreeWidget;
class QTreeWidgetItem;
class QPushButton;
class QCheckBox;
class QLineEdit;
class QProgressBar;
class QComboBox;

// класс - профодник по яндекс.диску
class CloudFileDialog : public QDialog
{
public:
    explicit CloudFileDialog(const QString& cloud, const QString& username, const QString& password, QWidget *parent = nullptr);
    ~CloudFileDialog() = default;

    bool setConnect() const { return m_connect; }
    QString getDownloadPath() const { return download_path; }
    QString getFileName() const { return file_name; }
    QString getFileExtansion() const { return file_extansion; }

protected:
    virtual void keyPressEvent(QKeyEvent* pe) override;

private:
    void getFolderList();
    void _fillComboBoxType();

public slots:
    void printFolderList();

    void selectItem(QTreeWidgetItem* tw_item);
    void changeItem();
    void clickUp();
    void clickOK();
    void downloadFile();
    void slotDownloadProgress(qint64 nReceived, qint64 nTotal);

private:
    QLineEdit* le_path;
    QPushButton* pb_up;
    QTreeWidget* w_tree;
    QLineEdit* le_filename;
    QComboBox* cb_filetype;
    QPushButton* pb_open;
    QPushButton* pb_cancel;
    QCheckBox* cb_openfolder;
    QProgressBar* prb_download;

    QWebdav w;
    QWebdavDirParser p;
    QString m_path;    

    QString download_path;
    QString file_name;
    QString file_extansion;

    bool m_connect;
};

#endif // CLOUDFILEDIALOG_H
