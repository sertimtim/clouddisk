#include "LinkUI.h"
#include <QLineEdit>
#include <QLabel>
#include <QLayout>

LinkUI::LinkUI(QWidget *parent) : QWidget(parent)
{
    QLabel* lbl_fileID = new QLabel("Specify a link to the file:");
    le_link = new QLineEdit;
    le_link->setWhatsThis("Specify a link to the file.\n"
                            "For example: https://yadi.sk/i/jESK5J_xxxxxxx");
    // "For example: https://drive.google.com/file/d/1oFVr6J6QK59yn2TqfoBk_XXXXX-XXXXX\n"
    // "Or file_id (only for Google): 1oFVr6J6QK59yn2TqfoBk_XXXXX-XXXXX"
    QLabel* lbl_example = new QLabel("For help, click \"?\"");

    QVBoxLayout* lo_all = new QVBoxLayout;
    lo_all->addWidget(lbl_fileID);
    lo_all->addWidget(le_link);
    lo_all->addWidget(lbl_example);

    setLayout(lo_all);
}

QString LinkUI::getLink() const
{
    return le_link->text();
}
